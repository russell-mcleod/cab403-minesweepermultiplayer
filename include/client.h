#include <stdbool.h>
#include "game.h"

int socket_setup(const char* addr, const char* port);
int8_t login(char *user_name, char *password);
int get_menu(void);
int get_game_choice();
bool is_game_over();
int close_connection(int socket_to_close);
void sig_handler(int signo);
int enter_coords(Command *cmd);
void shutdown_exit();