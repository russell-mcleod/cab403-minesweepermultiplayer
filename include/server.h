#include "game.h"
#define RANDOM_NUMBER_SEED 42
#define BACKLOG 10
#define MAX_CLIENTS 10

void safe_srand(int seed);
int receive_login();
int receive_game_data(int socket_id, Gamestate *state);
int close_connection(int socket_to_close);
void sig_handler(int signo);
void *gameroom_start(void *thread_num);
int add_socket(int socket_id);
int eat_socket(int *socket_id);
void shutdown_child_client_socket(void *socket_id);
