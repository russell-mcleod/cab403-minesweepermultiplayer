#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#define NUM_TILE_X 9
#define NUM_TILE_Y 9
#define NUM_MINES 10

typedef struct {
    char command[2];
    int x;
    int y;
} Command;

typedef struct {
    int adjacent_mines;
    bool revealed;
    bool is_mine;
    bool is_flagged;
} Tile;

typedef struct {
    Tile tiles[NUM_TILE_X][NUM_TILE_Y];
    int mines_left;
    bool game_over;
    time_t time;
} Gamestate;

typedef struct {
    char name[64];
    int games_won;
    int games_played;
} Player;

typedef struct {
    Player player;
    int time;
} Score;

typedef struct {
    Score scores[100];
    int score_count;
} Leaderboard;

bool tile_contains_mine(Gamestate *state, int x, int y);
void place_mines(Gamestate *state);
int get_adj_mine_count(Gamestate *state, int x, int y);
bool has_hit_mine(Gamestate *state);
void reset_gamestate(Gamestate *state);
Gamestate create_game();
void print_field(Gamestate *state, bool view_hidden);
void print_revealed(Gamestate *state);
void print_flagged(Gamestate *state);
Gamestate get_field_client_view(Gamestate *state);
bool is_revealed(Gamestate *state, int x, int y);
bool is_flagged(Gamestate *state, int x, int y);
void reveal_tile(Gamestate *state, int x, int y);
void reveal_mines(Gamestate *state);
void place_flag(Gamestate *state, int x, int y);
void print_leaderboard(Leaderboard *lb);
void decode_coord(Command *cmd, char *coord);