IDIR = include
SRCDIR = src
OBJDIR = obj
OUTDIR = bin
CC=gcc
CFLAGS=-I$(IDIR)

all: pre _game _server _client serverbin clientbin

pre:
	mkdir -p bin
	mkdir -p obj

server: _game _server serverbin

client: _game _client clientbin

make: all

serverbin:
	$(CC) -o $(OUTDIR)/server $(OBJDIR)/game.o $(OBJDIR)/server.o -pthread
	cp doc/Authentication.txt bin/Authentication.txt

clientbin:
	$(CC) -o $(OUTDIR)/client $(OBJDIR)/game.o $(OBJDIR)/client.o

_game:
	$(CC) -c $(SRCDIR)/game.c $(CFLAGS) -o $(OBJDIR)/game.o

_server:
	$(CC) -c $(SRCDIR)/server.c $(CFLAGS) -o $(OBJDIR)/server.o

_client:
	$(CC) -c $(SRCDIR)/client.c $(CFLAGS) -o $(OBJDIR)/client.o

clean:
	rm -rf $(OUTDIR)
	rm -rf $(OBJDIR)
