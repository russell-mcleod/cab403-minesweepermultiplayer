#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "client.h"

int sockFd;
struct sockaddr_in server;

Gamestate gamestate;
Command cmd;
Leaderboard leaderboard;
bool is_logged_in = false;

// Main function
int main(int argc, char const *argv[]) {
    // Setup function to run when Ctrl+C is pressed
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("Cannot catch SIGINT\n");
    
    if(argc != 3) {
        printf("Please include the hostname and the port as arguments\n");
        exit(0);
    }

    // Connect to server
    socket_setup(argv[1], argv[2]);
    int ret = connect(sockFd, (struct sockaddr *)&server, sizeof(server));
    if (ret < 0) // If conenct fails then close socket and exit program
    {
        printf("Failed to connect to game server! Exiting...\n");
        close_connection(sockFd);
        exit(0);
    }

    printf("========================================================\n");
    printf("Welcome to the online Minesweeper gaming system\n");
    printf("========================================================\n\n");
    
    // GEt login details from user and try to login
    char user_name[64];
    char password[64];
    int c;

    bzero(&user_name, sizeof(user_name));
    bzero(&password, sizeof(password));
    printf("You are required to log on with your registered user name and password\n\n");
    printf("Username: ");
    scanf(" %63s", user_name);
    while((c = getchar()) != '\n' && c != EOF); // eat extra garbage chars put into stdin
    printf("Password: ");
    scanf(" %63s", password);
    while((c = getchar()) != '\n' && c != EOF); // eat extra garbage chars put into stdin
    
    // If login fails then show error message, disconnect from server, and exit
    if (login(user_name, password) != 0) {
        printf("You entered either an incorrect username or password. Disconnecting.\n");
        strcpy(cmd.command, "3");
        cmd.x = 0;
        cmd.y = 0;
        send(sockFd, &cmd, sizeof(cmd), 0);
        close_connection(sockFd);
        return -1;
    } else {
        is_logged_in = true;
        printf("Login successful. Welcome %s!\n\n", user_name);        
    }
    // Main client-side game loop
    while (get_menu() == 0);
    // Close connection when quitting game
    close_connection(sockFd);
    return 0;
}

// Setup socket for connecting to server
int socket_setup(const char* addr, const char* port) {
    sockFd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&server, sizeof(server));
    inet_pton(AF_INET, addr, &server.sin_addr);
    server.sin_family = AF_INET;
    server.sin_port = htons(atoi(port));

    return 0;
}

// Login function to send login to server and return result
int8_t login(char *user_name, char *password) {
    int ret;
    int result = -1;
    
    // Send username and password to server
    printf("Sending Username and password...\n");
    printf("Please wait for an available game room to become free on the server...\n");
    char userpass[129];
    bzero(&userpass, sizeof(userpass));
    strcpy(userpass, user_name);
    strcat(userpass, "\n");
    strcat(userpass, password);

    if (send(sockFd, &userpass, sizeof(userpass), 0) < 0) {
        printf("Sending Username and Password FAILED! Exiting...\n");
        shutdown_exit();     // something went wrong, close program
    }

    // Receive login result from server
    if (recv(sockFd, &result, sizeof(result), 0) < 1) {
        printf("Receiving response from Server FAILED! Exiting...\n");
        shutdown_exit();     // something went wrong, close program
    }

    return result;
}

// Main menu function to startgame/view leaderboard/quit game
int get_menu(void) {
    char selection[8];
    int c;
    bzero(&selection, sizeof(selection));
    bzero(&cmd, sizeof(cmd));

    // Show menu and get choice from user
    printf("Welcome to the Minesweeper gaming system.\n\n");
    printf("Please enter a selection:\n<1> Play Minesweeper\n<2> Show Leaderboard\n<3> Quit\n\n");
    printf("Seletion option (1-3): ");
    scanf(" %2s", &selection);
    while((c = getchar()) != '\n' && c != EOF)    ; // eat extra garbage chars put into stdin

    // Process menu choice and send command to server
    if (strlen(selection) == 1)
        switch (atoi(selection)) {
            case 1:                                 // Start new minesweeper game
                strcpy(cmd.command, selection);
                cmd.x = 0;
                cmd.y = 0;
                if (send(sockFd, &cmd, sizeof(cmd), 0) < 0) {
                    printf("Error sending data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }

                if (recv(sockFd, &gamestate, sizeof(gamestate), 0) < 1) {
                    printf("Error receiving data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }
                    
                do {
                    print_field(&gamestate, false);
                } while (get_game_choice() == 0);
            
                break;
            case 2:                                 // Show leaderboard
                // Print default leaderboard since leaderboard modification is not implemented
                print_leaderboard(&leaderboard);
                break;
            case 3:                                 // Quit program
                printf("Quit\n");
                strcpy(cmd.command, selection);
                cmd.x = 0;
                cmd.y = 0;
                send(sockFd, &cmd, sizeof(cmd), 0);
                return -1;

            default:
                printf("Invalid selection: %s!\n", selection);
                break;
        }
    else
        printf("Invalid selection: %s!\n", selection);

    return 0;
}

// Function to process minesweeper move choice and send to server
int get_game_choice() {
    int c;
    bzero(&cmd, sizeof(cmd));

    // Display game comamnd menu and receive choice from user
    printf("\n\nChoose an option:\n");
    printf("<R> Reveal tile\n");
    printf("<P> Place flag\n");
    printf("<Q> Quit game\n");
    printf("\nOption (R,P,Q): ");
    
    scanf(" %2s", &cmd.command);
    while((c = getchar()) != '\n' && c != EOF)    ; // eat extra garbage chars put into stdin

    // Process user choice and send command to server
    if (strlen(cmd.command) == 1) {
        if (strcmp(cmd.command, "P") == 0 || strcmp(cmd.command, "p") == 0) {        // Place a Flag
            // Infinite loop until user enters valid coordinates
            while (enter_coords(&cmd) != 0);

            // If specified tile is already flagged or revealed then tell user
            // Otherwise send command to server and get response
            if (is_revealed(&gamestate, cmd.x, cmd.y))
                printf("This tile is already revealed!\n");
            else if (is_flagged(&gamestate, cmd.x, cmd.y))
                printf("This tile is already flagged!\n");
            else {
                if (send(sockFd, &cmd, sizeof(cmd), 0) < 0) {
                    printf("Error sending data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }

                if (recv(sockFd, &gamestate, sizeof(gamestate), 0) < 1) {
                    printf("Error receiving data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }
            }

            // If game is won then display message and return to main menu
            if (gamestate.mines_left == 0) {
                print_field(&gamestate, true);
                printf("Congratulations! You have located all the mines.\n");
                printf("Time taken: %ld seconds.\n\n", gamestate.time);
                return -1;
            }
        } else if (strcmp(cmd.command, "R") == 0 || strcmp(cmd.command, "r") == 0) { // Reveal a Tile
            // Infinite loop until valid coords are given
            while (enter_coords(&cmd) != 0);

            // If tile is already revealed then tell user
            // Otherwise send command to server and receive response
            if (is_revealed(&gamestate, cmd.x, cmd.y))
                printf("This tile is already revealed!\n");
            else {
                if (send(sockFd, &cmd, sizeof(cmd), 0) < 0) {
                    printf("Error sending data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }

                if (recv(sockFd, &gamestate, sizeof(gamestate), 0) < 1) {
                    printf("Error receiving data, Exiting...\n");
                    shutdown_exit();     // something went wrong, close program
                }

                // If game is over then tell user and return to main menu
                if (gamestate.game_over == true) {
                    print_field(&gamestate, true);
                    printf("You hit a mine. Game over.\n");
                    return -1;
                }
            }
        } else if(strcmp(cmd.command, "Q") == 0 || strcmp(cmd.command, "q") == 0) {  // Quit current minesweeper game
            // Tell server that user quit game, then return to main menu
            cmd.x = 0;
            cmd.y = 0;
            send(sockFd, &cmd, sizeof(cmd), 0);
            return -1;
        }
        else {
            printf("Invalid choice!\n");   
        }
    } else {
        printf("Invalid choice!\n");
    }

    return 0;
}

// Function to tell server that user has quit and close connection
int close_connection(int socket_to_close) {
    // If user is logged in...
    // ...then server is still waiting for command, so send User Exit Program command
    // Otherwise server is still waiting for login details, so send blacklisted/incorrect login
    if (is_logged_in) {
        strcpy(cmd.command, "3");
        cmd.x = 0;
        cmd.y = 0;
        send(socket_to_close, &cmd, sizeof(cmd), 0);
    } else {
        char userpass[129];
        strcpy(userpass, "Username\nQRRBRRBIRLBEL");
        send(socket_to_close, &userpass, sizeof(userpass), 0);
    }
    // Close connection
    int ret = shutdown(socket_to_close, 2);
    ret = close(socket_to_close);

    return ret;
}

// Function to run when Ctrl+C is pressed
void sig_handler(int signo) {
    if (signo == SIGINT) {
        shutdown_exit();
    }
}

// Function to close connection and exit program
void shutdown_exit() {
    printf("\nClient socket shutting down: %d\n", close_connection(sockFd));
    exit(0);
}

// Function to get user to enter game field coordinates and process them
int enter_coords(Command *cmd) {
    char choice_coords[3];
    int c;
    bzero(&choice_coords, sizeof(choice_coords));

    // Get coordinates from user
    printf("Enter tile coordinates: ");
    scanf(" %3s", &choice_coords);
    while((c = getchar()) != '\n' && c != EOF)    ; // eat extra garbage chars put into stdin

    if (strlen(choice_coords) != 2) {
        printf("Invalid coordinates!\n");
        return -1;
    }

    // Decode user's letter-number coordinates to x,y format and add to command
    decode_coord(cmd, choice_coords);

    if (cmd->x == -1 || cmd->y == -1) {
        printf("Invalid coordinates!\n");
        return -1;
    }

    return 0;
}