#include <arpa/inet.h>
#include <signal.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include "server.h"

int sockfd, new_conn;
struct sockaddr_in my_addr;
struct sockaddr_in their_addr;
FILE *fp;
char user[64];
uint8_t selection;

Leaderboard leaderboard;

pthread_t game_rooms[MAX_CLIENTS];
int socket_queue[MAX_CLIENTS];
pthread_mutex_t srand_lock;
pthread_mutex_t socket_queue_lock;

sem_t full;
sem_t empty;
int insertPointer = 0, removePointer = 0;

// Function to access srand() safely using mutex lock
void safe_srand(int seed) {
    pthread_mutex_lock(&srand_lock);
    srand(seed);
    pthread_mutex_unlock(&srand_lock);
}

// Main function
int main(int argc, char const *argv[]) {
    int port;

    // Exit server if mutex/semaphore locks cannot be initialized
    if (pthread_mutex_init(&srand_lock, NULL) != 0)
    {
        printf("srand() mutex init failed, exiting...\n");
        return 1;
    }

    if (pthread_mutex_init(&srand_lock, NULL) != 0)
    {
        printf("Socket queue mutex init failed, exiting...\n");
        return 1;
    }

    if (sem_init(&empty, 0, MAX_CLIENTS) != 0) {
        printf("Socket queue empty semaphore init failed, exiting...\n");
        return 1;
    }

    if (sem_init(&full, 0, 0) != 0) {
        printf("Socket queue full semaphore init failed, exiting...\n");
        return 1;
    }

    // Setup function to run when Ctrl+C is pressed
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("Cannot catch SIGINT\n");
    
    // if port not specified, then use default port 12345
    if(argc != 2) {
        port = 12345; 
    } else {
	    port = atoi(argv[1]);
    }

    socklen_t sin_size;

    // Create a socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

    my_addr.sin_family = AF_INET;           // host byte order
	my_addr.sin_addr.s_addr = INADDR_ANY;   // auto-fill with server IP
    my_addr.sin_port = htons(port);         // short, network byte order

    // Bind the socket to the end point
	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		exit(1);
	}

    // Start listnening
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

    printf("Server is listnening on port %d...\n", ntohs(my_addr.sin_port));

    // Create thread pool of 10 threads
    for (int i = 0; i < MAX_CLIENTS; i++) {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        int *arg = malloc(sizeof(int));
        *arg = i+1;
        printf("Creating thread %d of %d... ", i+1, MAX_CLIENTS);
        printf("Returned %d\n", pthread_create(&game_rooms[i], &attr, gameroom_start, arg));
    }

    // Main server loop
    while(1) {
        sin_size = sizeof(struct sockaddr_in);
        if ((new_conn = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
			perror("accept");
			continue;
		}
        printf("server: got connection from %s\n", inet_ntoa(their_addr.sin_addr));

        // Add connected socket to socket queue for child threads
        if(add_socket(new_conn))
            fprintf(stderr, "Error adding socket to queue.");
    }
    return 0;
}

// Function to receive login data over a socket and return result of login
int receive_login(int socket_id) {
    char recv_userpass[129];
    char recv_user_name[64];
    char recv_password[64];
    char buf[1024];
    char comp_user_name[64];
    char comp_password[64];
    int bytes;
    int is_correct = -1;
    
    // Receive login data from client
    printf("Waiting for Username and Password... ");
    bytes = recv(socket_id, &recv_userpass, sizeof(recv_userpass), 0);
    printf("%d\n", bytes);
    if (bytes < 1) {            // if failed to receive login, return Login Fail
        perror("recv");	 
        return -1;
	}

    // Check received login against valid credentials stored in file
    sscanf(recv_userpass, "%s%s", recv_user_name, recv_password);
    printf("%s\n", recv_user_name);
    printf("%s\n", recv_password);
    
    printf("Checking login against stored account data...\n");
    
    // Read credentials file
    fp = fopen("doc/Authentication.txt", "r");
    //This is here because the game can be started from multiple places
    if (fp == NULL) {
        fp = fopen("Authentication.txt", "r");
        if(fp == NULL) {        // if unable to open login data file, the return Login Fail
            perror("fopen() failed: Unable to open Authentication.txt!\n");
            return EXIT_FAILURE;
        }
    }
    
    // Check received login against file data
    while(fgets(buf, 40, fp) != NULL) {
        sscanf(buf, "%s%s", comp_user_name, comp_password);
        if(strcmp(comp_user_name, recv_user_name) == 0 && strcmp(comp_password, recv_password) == 0 
               && strcmp(comp_user_name, "Username") != 0) {
            is_correct = 0;
            break;
        } else {
            is_correct = -1;
        }
    }
    // Close file when done
    fclose(fp);

    // Send result to client and return result to caller
    if(is_correct == 0) {   // login ok
        printf("Correct username and password, user will now log in\n");
        strcpy(user, recv_user_name);
    } else {                // login wrong
        printf("Incorrect username or password\n");
    }
    printf("send: %ld\n", send(socket_id, &is_correct, sizeof(is_correct), 0));
    
    return is_correct;
}

// Function to receive and process game commands from client
int receive_game_data(int socket_id, Gamestate *state) {
    Command cmd;
    Gamestate clean_state;
    bzero(&cmd, sizeof(cmd));

    // Receive game command from client
    printf("Waiting for game data...\n ");
    recv(socket_id, &cmd, sizeof(cmd), 0);
    printf("Command: %s\n", cmd.command);
    printf("Coords: X = %d, Y = %d\n", cmd.x, cmd.y);

    // Do game logic basd on received command
    if (strcmp(cmd.command, "1") == 0) {            // Start new minesweeper game
        *state = create_game();                     // 1. Create new game state
        printf("Newly Created Game Field:\n");      // 2. Create alt state with unrevealed data scrubbed
        print_field(state, true);                   // 3. Send "clean" data to client
        clean_state = get_field_client_view(state);
        send(socket_id, &clean_state, sizeof(clean_state), 0);
    }

    else if (strcmp(cmd.command, "2") == 0) {       // Show Leaderboard
        // Not yet implemented
    }

    else if (strcmp(cmd.command, "3") == 0) {       // User Exited Program
        printf("Client quit program.\n");           // 1. Return -1 to stop receive data loop
        return -1;
    }

    else if (strcmp(cmd.command, "P") == 0 || strcmp(cmd.command, "p") == 0) {  // Place a Flag
        place_flag(state, cmd.x, cmd.y);                                        // 1. Place flag at position specified in command
        clean_state = get_field_client_view(state);                             // 2. Make "clean" scrubbed game state for client
                                                                                // 3. If game is won, then update time and send full state to client
        printf("\nFIELD VIEW [SERVER]:\n");                                     //    Otherwise send clean state to client
        print_field(state, true);
        printf("\nFIELD VIEW [CLIENT]:\n");
        print_field(&clean_state, true);

        if (state->mines_left == 0) {
            state->time = time(NULL) - state->time;
            printf("Game Time: %ld seconds\n", state->time);
            send(socket_id, state, sizeof(*state), 0);
        } else
            send(socket_id, &clean_state, sizeof(clean_state), 0);
    }

    else if (strcmp(cmd.command, "R") == 0 || strcmp(cmd.command, "r") == 0) {  // Reveal a Tile
        if (tile_contains_mine(state, cmd.x, cmd.y)) {                          // 1. If tile contains mine, set game over and reveal all mines
            state->game_over = true;                                            //    Otherwise just reveal tile specified in command
            reveal_mines(state);                                                // 2. Make "clean" scrubbed game state for client
        } else {                                                                // 3. If game is over then send full state to client
            reveal_tile(state, cmd.x, cmd.y);                                   //    Otherwise send clean state to slient
        }

        clean_state = get_field_client_view(state);
        printf("\nFIELD VIEW [SERVER]:\n");
        print_field(state, true);
        printf("\nFIELD VIEW [CLIENT]:\n");
        print_field(&clean_state, true);

        if (state->game_over == true)
            send(socket_id, state, sizeof(*state), 0);
            
        else
            send(socket_id, &clean_state, sizeof(clean_state), 0);
    }
    else if (strcmp(cmd.command, "Q") == 0 || strcmp(cmd.command, "q") == 0) {  // Quit (forfeit) current minesweeper game
        printf("Client quit game.\n");                                          // 1. Return and wait for client to send next command
    }
    
    return 0;
}

// Function to run when Ctrl+C is pressed
void sig_handler(int signo) {
    if (signo == SIGINT) {
        printf("\nServer cleaning up before exiting...\n");

        // Signal child threads to stop what they are doing, cleanup, and terminate
        for (int i = 0; i < MAX_CLIENTS; i++) {
            printf("Stopping thread %d of %d...\n", i+1, MAX_CLIENTS);
            pthread_cancel(game_rooms[i]);
        }

        // Shut down socket used by main server thread
        printf("Server socket shutting down: %d\n", close_connection(sockfd));
        printf("Server Exiting...\n");
        exit(0);
    }
}

// Wrapper function to close socket connection
int close_connection(int socket_to_close) {
    int ret = shutdown(socket_to_close, 2);
    ret = close(socket_to_close);

    return ret;
}

// Main thread run by child threads for managing game
void *gameroom_start(void *thread_num) {
    Gamestate state;
    Gamestate clean;
    int socket_id;
    int threadNo = *((int *)thread_num);
    free(thread_num);

    // Tell thread to run cleanup function before terminating
    int *arg = malloc(sizeof(int));
    *arg = socket_id;
    pthread_cleanup_push(shutdown_child_client_socket, arg);

    // Setup random seed
    safe_srand(RANDOM_NUMBER_SEED);
    
    // Main child thread loop
    while (1) {
        // Grab socket from queue
        if(eat_socket(&socket_id))
			fprintf(stderr, "Error consuming socket from queue.\n");
		else
			printf("Socket %d taken by Thread %d\n", socket_id, threadNo);

        // Process login from client
        int ret = receive_login(socket_id);
        printf("ret = %d\n", ret);
        if (ret == 0) {
            // if logged in, enter main game processing loop
            while (receive_game_data(socket_id, &state) == 0)    ;  // infinite loop until user decides to quit program
        }
        else
        {
            printf("Login failed\n");
        }
        // Close connection to current client socket
        printf("Closing connection to client.\n");
        shutdown(socket_id, 2);
        close(socket_id);
    }
    // Run cleanup function to close any open client socket
    pthread_cleanup_pop(1);
}

int add_socket(int socket_id) {
    //Acquire Empty Semaphore
	sem_wait(&empty);

	//Acquire mutex lock to protect buffer
	pthread_mutex_lock(&socket_queue_lock);

    //Add new socket to queue
	socket_queue[insertPointer++] = socket_id;
	insertPointer = insertPointer % MAX_CLIENTS;

	//Release mutex lock and full semaphore
	pthread_mutex_unlock(&socket_queue_lock);
    sem_post(&full);

	return 0;
}

int eat_socket(int *socket_id) {
	//Acquire Full Semaphore
	sem_wait(&full);
	
	//Acquire mutex lock to protect buffer
	pthread_mutex_lock(&socket_queue_lock);

    //Consume next available socket in queue
	*socket_id = socket_queue[removePointer];
	socket_queue[removePointer++] = -1;
	removePointer = removePointer % MAX_CLIENTS;

	//Release mutex lock and empty semaphore
	pthread_mutex_unlock(&socket_queue_lock);
	sem_post(&empty);

	return 0;
}

// Cleanup function for child thread to close client connection before terminating
void shutdown_child_client_socket(void *socket_id) {
    printf("Client socket shutting down: %d\n", close_connection(*((int *)socket_id)));
    free(socket_id);
}