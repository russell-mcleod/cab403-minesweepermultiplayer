#include <stdio.h> 
#include <stdlib.h> 
#include <stdbool.h>
#include <unistd.h>
#include <string.h> 
#include "game.h"

// Function to check if a specific tile contains a mine
bool tile_contains_mine(Gamestate *state, int x, int y) {
    return state->tiles[x][y].is_mine;
}

// Function to place mines when creating new game
void place_mines(Gamestate *state) {
    for(int i = 0; i < NUM_MINES; i++) {
        int x,y;
        do {
            x = rand() % NUM_TILE_X;
            y = rand() % NUM_TILE_Y;
        } while(tile_contains_mine(state, x, y));

        state->tiles[x][y].is_mine = true;
    }
}

// Function to get the no. of mines adjacent to a tile
int get_adj_mine_count(Gamestate *state, int x, int y) {
    int count = 0;

    // top-left adj
    if ((x-1) >= 0 && (y-1) >= 0)
        if (tile_contains_mine(state, x-1, y-1))
            count++;

    // top-middle adj
    if ((y-1) >= 0)
        if (tile_contains_mine(state, x, y-1))
            count++;

    // top-right adj
    if ((x+1) < NUM_TILE_X && (y-1) >= 0)
        if (tile_contains_mine(state, x+1, y-1))
            count++;

    // middle-left adj
    if ((x-1) >= 0)
        if (tile_contains_mine(state, x-1, y))
            count++;

    // middle-right adj
    if ((x+1) < NUM_TILE_X)
        if (tile_contains_mine(state, x+1, y))
            count++;

    // bottom-left adj
    if ((x-1) >= 0 && (y+1) < NUM_TILE_Y)
        if (tile_contains_mine(state, x-1, y+1))
            count++;

    // bottom-middle adj
    if ((y+1) < NUM_TILE_Y)
        if (tile_contains_mine(state, x, y+1))
            count++;

    // bottom-right adj
    if ((x+1) < NUM_TILE_X && (y+1) < NUM_TILE_Y)
        if (tile_contains_mine(state, x+1, y+1))
            count++;

    return count;
}

// Function to check if a mine has been hit (lose game)
bool has_hit_mine(Gamestate *state) {
    return (state-> game_over && state->mines_left > 0);
}

// Function to print a game field to console
void print_field(Gamestate *state, bool view_hidden) {
    printf("\nRemaining Mines: %d\n", (*state).mines_left);
    printf("    1 2 3 4 5 6 7 8 9\n");
    printf("---------------------\n");

    for (int y = 0; y < NUM_TILE_Y; y++) {
        printf("%c | ", 0x41 + y);
        for (int x = 0; x < NUM_TILE_X; x++) {
            if (!state->tiles[x][y].is_flagged) {
                if ((!view_hidden && !(state->tiles[x][y].revealed)) || state->tiles[x][y].adjacent_mines == -1)
                    printf("  ");
                else if ((*state).tiles[x][y].is_mine)
                    printf("* ");
                else
                    printf("%d ", (*state).tiles[x][y].adjacent_mines);
            } else {
                if (has_hit_mine(state))    // if game is lost...
                    printf("* ");           // ...then printing mine takes priority over flag
                else                        // otherwise...
                    printf("+ ");           // ...print the flag instead
            }
        }
        printf("\n");
    }
}

// Function to print which tiles have been revealed by player
void print_revealed(Gamestate *state) {
    printf("\nRemaining Mines: %d\n", (*state).mines_left);
    printf("    1 2 3 4 5 6 7 8 9\n");
    printf("---------------------\n");

    for (int y = 0; y < NUM_TILE_Y; y++) {
        printf("%c | ", 0x41 + y);
        for (int x = 0; x < NUM_TILE_X; x++) {
            if (!(state->tiles[x][y].revealed))
                printf("  ");
            else
                printf("R ");

        }
        printf("\n");
    }
}

// Function to print which tiles have been flagged by player
void print_flagged(Gamestate *state) {
    printf("\nRemaining Mines: %d\n", (*state).mines_left);
    printf("    1 2 3 4 5 6 7 8 9\n");
    printf("---------------------\n");

    for (int y = 0; y < NUM_TILE_Y; y++) {
        printf("%c | ", 0x41 + y);
        for (int x = 0; x < NUM_TILE_X; x++) {
            if (!(state->tiles[x][y].is_flagged))
                printf("  ");
            else
                printf("+ ");

        }
        printf("\n");
    }
}

// Function that creates a copy of a game state that has been scrubbed of data for unrevealed times
Gamestate get_field_client_view(Gamestate *state) {
    Gamestate g;

    // Copy game state
    g.mines_left = state->mines_left;
    g.game_over = state->game_over;

    for (int y = 0; y < NUM_TILE_Y; y++) {
        for (int x = 0; x < NUM_TILE_X; x++) {
            // Copy tile state
            g.tiles[x][y].adjacent_mines = state->tiles[x][y].adjacent_mines;
            g.tiles[x][y].is_mine = state->tiles[x][y].is_mine;
            g.tiles[x][y].revealed = state->tiles[x][y].revealed;
            g.tiles[x][y].is_flagged = state->tiles[x][y].is_flagged;

            // if Tile is not revealed, scrub it "clean"
            if (!(g.tiles[x][y].revealed)) {
                g.tiles[x][y].adjacent_mines = -1;
                g.tiles[x][y].is_mine = false;
            }
        }
    }

    return g;
}

// Function that resets a game state to 0 before game creation
void reset_gamestate(Gamestate *state) {
     for(int x = 0; x < NUM_TILE_X; x++) {
        for(int y = 0; y < NUM_TILE_Y; y++) {
            state->tiles[x][y].adjacent_mines = 0;
            state->tiles[x][y].is_mine = 0;
            state->tiles[x][y].revealed = 0;
        }
    }
    state->mines_left = 10;
    state->game_over = false;
}

// Function that creates and sets up a new game state
Gamestate create_game() {
    Gamestate state;
    reset_gamestate(&state);    // make sure state is not garbage data
    place_mines(&state);
    for (int y = 0; y < NUM_TILE_Y; y++)
        for (int x = 0; x < NUM_TILE_X; x++) {
            state.tiles[x][y].adjacent_mines = get_adj_mine_count(&state, x, y);
            state.tiles[x][y].is_flagged = false;
            state.tiles[x][y].revealed = false;
        }
    state.mines_left = 10;
    state.time = time(NULL);
    return state;
}

// Function to check if a tile has been revealed
bool is_revealed(Gamestate *state, int x, int y) {
    return state->tiles[x][y].revealed;
}

// Function to check if a tile has been flagged
bool is_flagged(Gamestate *state, int x, int y) {
    return state->tiles[x][y].is_flagged;
}

// Function that reveals a tile on the game field at specified position
void reveal_tile(Gamestate *state, int x, int y) {
    state->tiles[x][y].revealed = true;
    state->tiles[x][y].is_flagged = false; // revealing a tile removes flag

    if (state->tiles[x][y].adjacent_mines == 0) {
        // top-left adj
        if ((x-1) >= 0 && (y-1) >= 0) 
            if (!tile_contains_mine(state, x-1, y-1) && !is_revealed(state, x-1, y-1))
                reveal_tile(state, x-1, y-1);

        // // top-middle adj
        if ((y-1) >= 0)
            if (!tile_contains_mine(state, x, y-1) && !is_revealed(state, x, y-1))
                reveal_tile(state, x, y-1);

        // // top-right adj
        if ((x+1) < NUM_TILE_X && (y-1) >= 0 && !is_revealed(state, x+1, y-1))
            if (!tile_contains_mine(state, x+1, y-1))
                reveal_tile(state, x+1, y-1);

        // // middle-left adj
        if ((x-1) >= 0)
            if (!tile_contains_mine(state, x-1, y) && !is_revealed(state, x-1, y))
                reveal_tile(state, x-1, y);

        // middle-right adj
        if ((x+1) < NUM_TILE_X)
            if (!tile_contains_mine(state, x+1, y) && !is_revealed(state, x+1, y))
                reveal_tile(state, x+1, y);

        // // bottom-left adj
        if ((x-1) >= 0 && (y+1) < NUM_TILE_Y)
            if (!(tile_contains_mine(state, x-1, y+1)) && !is_revealed(state, x-1, y+1))
                reveal_tile(state, x-1, y+1);

        // // bottom-middle adj
        if ((y+1) < NUM_TILE_Y)
            if (!tile_contains_mine(state, x, y+1) && !is_revealed(state, x, y+1))
                reveal_tile(state, x, y+1);

        // // bottom-right adj
        if ((x+1) < NUM_TILE_X && (y+1) < NUM_TILE_Y)
            if (!tile_contains_mine(state, x+1, y+1) && !is_revealed(state, x+1, y+1))
                reveal_tile(state, x+1, y+1);
    }
}

// Function that reveals all mines on the game field
void reveal_mines(Gamestate *state) {
    for (int y = 0; y < NUM_TILE_Y; y++) {
        for (int x = 0; x < NUM_TILE_X; x++) {
            if (tile_contains_mine(state, x, y))
                state->tiles[x][y].revealed = true;
        }
    }
}

// Function that places a flag on the game field at specified position
void place_flag(Gamestate *state, int x, int y) {
    if (!state->tiles[x][y].is_flagged) {
        state->tiles[x][y].is_flagged = true;
        if (tile_contains_mine(state, x, y)) {
            state->mines_left--;
        }
    }
    if (state->mines_left == 0)
        state->game_over = true;
}

// Function that prints a leaderboard to console
void print_leaderboard(Leaderboard *lb) {
    printf("\n=============================================================================\n\n");

    if (lb->score_count == 0)
        printf("There is no information currently stored in the leaderboard. Try again Later.\n");
    else
        for (int i = 0; i < lb->score_count; i++) {
            printf("%s\t\t%d seconds\t%d games won, %d games played\n", lb->scores[i].player.name, lb->scores[i].time, lb->scores[i].player.games_won, lb->scores[i].player.games_played);
        }

    printf("\n=============================================================================\n\n");
}

// Function that decodes a letter-number coordinate and adds it to game command
void decode_coord(Command *cmd, char *coord) {
    if((int)coord[0] > 0x60 && (int)coord[0] < 0x6A)        // lower case letter a-i
        cmd->y = (int)coord[0] - 0x61;
    else if ((int)coord[0] > 0x40 && (int)coord[0] < 0x4A)  // upper case letter A-I
        cmd->y = (int)coord[0] - 0x41;
    else
        cmd->y = -1;    // invalid 

    if ((int)coord[1] > 0x30 && (int)coord[1] < 0x3A)       // valid x coord 1-9
        cmd->x = (int)coord[1] - 0x31;
    else
        cmd->x = -1;        // invalid coord
}